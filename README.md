# ShopBridge

**Project Structure:**

- Backend project (.Net Core) is named as **ShopBridge.API**.

- Frontend project (Angular 9) is named as **ShopBridge.Client/shop-bridge-app**.

- Database script is named as **ShopBridgeDatabase.sql**.
----

# Steps to run the sample:

1. Generate database by running the sql script in SSMS.

1. Update the **App.Config** file in ShopBridge.API project to update the data part of connection string as per the local server (current value **data source=laptop-7dmmaje7**)

1. Open the basckend project in VisualStudio and start the application.

1. Open the forntend project in VisualStudio Code and run the **ng serve --open** command in terminal.