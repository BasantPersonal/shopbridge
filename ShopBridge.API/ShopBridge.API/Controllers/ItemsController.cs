﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopBridge.Data.Models;
using ShopBridge.Services;

namespace ShopBridge.API.Controllers
{
    [Route("api/items")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        IITemsService itemsService;
        public ItemsController(IITemsService itemsService)
        {
            this.itemsService = itemsService;
        }

        [HttpGet]
        public async Task<ActionResult<List<Item>>> GetItems()
        {
            return await itemsService.GetItems();
        }

        [HttpPost]
        public async Task<ActionResult<Item>> AddItem([FromBody]Item item)
        {
            return await itemsService.AddItem(item);
        }

        [HttpDelete("{name}")]
        public async Task<ActionResult> DeleteItem(string name)
        {
            await itemsService.DeleteItem(name);
            return Ok();
        }
    }
}