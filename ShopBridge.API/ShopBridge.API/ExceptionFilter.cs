﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridge.API
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is KeyNotFoundException || context.Exception is ArgumentException)
            {
                // handle explicit 'known' API errors
                context.HttpContext.Response.StatusCode = 401;
            }
            else
            {
                context.HttpContext.Response.StatusCode = 401;

            }
            var msg = context.Exception.Message;
            context.Result = new JsonResult(msg);
            base.OnException(context);
        }
    }
}
