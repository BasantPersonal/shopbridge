﻿using ShopBridge.Data;
using ShopBridge.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge.Services
{
    public interface IITemsService
    {
        Task<Item> AddItem(Item item);
        Task<List<Item>> GetItems();
        Task DeleteItem(string name);
    }
    public class ItemsService : IITemsService
    {
        private ShopBridgeEntities dbContext;
        public ItemsService(IShopBridgeContext context)
        {
            dbContext = context as ShopBridgeEntities;
        }

        public async Task<Item> AddItem(Item item)
        {
            if(string.IsNullOrEmpty(item.Name))
            {
                throw new ArgumentNullException(nameof(item.Name));
            }
            else if (string.IsNullOrEmpty(item.Description))
            {
                throw new ArgumentNullException(nameof(item.Description));
            }
            else if (item.Price <= 0)
            {
                throw new ArgumentException("Price should be greater than 0.");
            }

            if(dbContext.DbItems.Any(x=>x.Name.ToLower() == item.Name.ToLower().Trim()))
            {
                throw new ArgumentException("An item with the same name already exists.");
            }
            else
            {
                var savedItem = dbContext.DbItems.Add(item.ToDbItem());
                await dbContext.SaveChangesAsync();
                return new Item(savedItem);
            }
        }

        public async Task DeleteItem(string name)
        {
            var item = dbContext.DbItems.FirstOrDefault(x => x.Name.ToLower() == name.ToLower().Trim());
            if (item == null)
                throw new KeyNotFoundException($"No item found with name: {name}");
            else
            {
                dbContext.DbItems.Remove(item);
                await dbContext.SaveChangesAsync();
            }
        }

        public async Task<List<Item>> GetItems()
        {
            return (await dbContext.DbItems.ToListAsync()).Select(x => new Item(x)).ToList();
        }
    }
}
