﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge.Data
{
    public interface IShopBridgeContext : IDisposable
    {
        DbSet<DbItem> DbItems { get; set; }
    }
}
