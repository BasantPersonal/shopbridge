﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge.Data.Models
{
    public class Item
    {
        #region C'tors
        public Item() { }
        public Item(DbItem dbItem)
        {
            if (dbItem != null)
            {
                this.Name = dbItem.Name;
                this.Description = dbItem.Description;
                this.Price = dbItem.Price;
            }
        }
        #endregion
        #region Object Model
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        #endregion
        public DbItem ToDbItem()
        {
            return new DbItem()
            {
                Name = this.Name.Trim(),
                Description = this.Description.Trim(),
                Price = this.Price
            };
        }
    }
}
